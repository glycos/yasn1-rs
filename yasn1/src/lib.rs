#![cfg_attr(feature = "serde", feature(specialization))]
#![cfg_attr(nightly, feature(iter_rfind))]

extern crate untrusted;
extern crate num_bigint;
extern crate num_traits;
extern crate chrono;

#[cfg(feature="serde")]
extern crate serde;

pub mod prelude;

pub mod types;

mod der;

pub use der::Der;

pub enum Asn1 {
    Boolean(bool),
    Integer(i64), // TODO: Optional biginteger support
    BigInteger(num_bigint::BigInt),
    BitString,
    OctetString(Vec<u8>),
    Null,
    OID(types::Oid),
    Real(f64),
    Enum(u32),
    Sequence(Vec<Asn1>),
    UTCTime(chrono::DateTime<chrono::Utc>),
    GeneralizedTime(chrono::DateTime<chrono::Utc>),
}
