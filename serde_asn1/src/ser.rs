use std;
use serde::ser;

use asn1;
use asn1::prelude::*;

use error::{Error, Result};

pub struct Serializer;

impl ser::Serializer for Serializer {
    type Ok = Asn1;
    type Error = Error;

    type SerializeSeq = SerializeSeq;
    type SerializeMap = SerializeSeq;
    type SerializeTuple = SerializeSeq;
    type SerializeTupleStruct = SerializeSeq;
    type SerializeTupleVariant = SerializeSeq;
    type SerializeStruct = SerializeSeq;
    type SerializeStructVariant = SerializeSeq;

    fn serialize_bool(self, el: bool) -> Result<Asn1> {
        Ok(Asn1::Boolean(el))
    }

    fn serialize_i8(self, el: i8) -> Result<Asn1> {
        self.serialize_i64(el as i64)
    }

    fn serialize_i16(self, el: i16) -> Result<Asn1> {
        self.serialize_i64(el as i64)
    }

    fn serialize_i32(self, el: i32) -> Result<Asn1> {
        self.serialize_i64(el as i64)
    }

    fn serialize_u8(self, el: u8) -> Result<Asn1> {
        self.serialize_i64(el as i64)
    }

    fn serialize_u16(self, el: u16) -> Result<Asn1> {
        self.serialize_i64(el as i64)
    }

    fn serialize_u32(self, el: u32) -> Result<Asn1> {
        self.serialize_i64(el as i64)
    }

    fn serialize_u64(self, el: u64) -> Result<Asn1> {
        self.serialize_i64(el as i64)
    }

    fn serialize_i64(self, el: i64) -> Result<Asn1> {
        Ok(Asn1::Integer(el))
    }

    fn serialize_f32(self, el: f32) -> Result<Asn1> {
        self.serialize_f64(el as f64)
    }

    fn serialize_f64(self, el: f64) -> Result<Asn1> {
        Ok(Asn1::Real(el))
    }

    fn serialize_none(self) -> Result<Asn1> {
        Err(Error)
    }

    fn serialize_some<T: ?Sized>(self, val: &T) -> Result<Asn1> {
        Err(Error)
    }

    fn serialize_seq(self, len: Option<usize>) -> Result<SerializeSeq> {
        match len {
            Some(len) => Ok(SerializeSeq::with_capacity(len)),
            None => Ok(SerializeSeq::with_capacity(0)),
        }
    }

    fn serialize_map(self, len: Option<usize>) -> Result<SerializeSeq> {
        match len {
            Some(len) => Ok(SerializeSeq::with_capacity(len)),
            None => Ok(SerializeSeq::with_capacity(0)),
        }
    }

    fn serialize_unit(self) -> Result<Asn1> {
        Err(Error)
    }

    fn serialize_unit_struct(self, name: &str) -> Result<Asn1> {
        Err(Error)
    }

    fn serialize_char(self, c: char) -> Result<Asn1> {
        Err(Error)
    }

    fn serialize_str(self, c: &str) -> Result<Asn1> {
        self.serialize_bytes(c.as_bytes())
    }

    fn serialize_bytes(self, b: &[u8]) -> Result<Asn1> {
        Ok(Asn1::OctetString(Vec::from(b)))
    }

    fn serialize_newtype_struct<T: ?Sized>(self, name: &str, value: &T) -> Result<Asn1>
        where T: ser::Serialize
    {
        value.serialize(self)
    }

    fn serialize_newtype_variant<T: ?Sized>(self, name: &str, index: u32, variant: &str, value: &T) -> Result<Asn1>
        where T: ser::Serialize
    {
        //value.serialize(self)
        Err(Error)
    }

    fn serialize_struct(self, name: &str, len: usize) -> Result<SerializeSeq> {
        Ok(SerializeSeq::with_capacity(len))
    }

    fn serialize_tuple_struct(self, name: &str, len: usize) -> Result<SerializeSeq> {
        Ok(SerializeSeq::with_capacity(len))
    }

    fn serialize_tuple(self, len: usize) -> Result<SerializeSeq> {
        Ok(SerializeSeq::with_capacity(len))
    }

    fn serialize_unit_variant(self, _name: &str, index: u32, _variant: &str) -> Result<Asn1> {
        Ok(Asn1::Enum(index))
    }

    fn serialize_tuple_variant(self, _name: &str, index: u32, _variant: &str, len: usize) -> Result<SerializeSeq> {
        // TODO: find out how we want to implement this
        Err(Error)
    }

    fn serialize_struct_variant(self, _name: &str, index: u32, _variant: &str, len: usize) -> Result<SerializeSeq> {
        // TODO: find out how we want to implement this
        Err(Error)
    }
}

impl asn1::types::OidSerializer for Serializer {
    fn serialize_oid(self, oid: &Oid) -> Result<Self::Ok> {
        // TODO: it should definitely be possible to have a reference in Asn1
        Ok(Asn1::OID(oid.clone()))
    }
}

pub struct SerializeSeq {
    contents: Vec<Asn1>,
}

impl SerializeSeq {
    fn with_capacity(len: usize) -> Self {
        SerializeSeq {
            contents: Vec::with_capacity(len),
        }
    }
}

impl ser::SerializeSeq for SerializeSeq {
    type Ok = Asn1;
    type Error = Error;

    fn serialize_element<T: ?Sized>(&mut self, el: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(el)?);
        Ok(())
    }

    fn end(self) -> Result<Asn1> {
        Ok(Asn1::Sequence(self.contents))
    }
}

impl ser::SerializeMap for SerializeSeq {
    type Ok = Asn1;
    type Error = Error;

    fn serialize_key<T: ?Sized>(&mut self, key: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(key)?);
        Ok(())
    }

    fn serialize_value<T: ?Sized>(&mut self, value: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(value)?);
        Ok(())
    }

    fn end(self) -> Result<Asn1> {
        Ok(Asn1::Sequence(self.contents))
    }
}

impl ser::SerializeStruct for SerializeSeq {
    type Ok = Asn1;
    type Error = Error;

    fn serialize_field<T: ?Sized>(&mut self, _/*key*/: &str, el: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(el)?);
        Ok(())
    }

    fn end(self) -> Result<Asn1> {
        Ok(Asn1::Sequence(self.contents))
    }
}

impl ser::SerializeTupleStruct for SerializeSeq {
    type Ok = Asn1;
    type Error = Error;

    fn serialize_field<T: ?Sized>(&mut self, el: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(el)?);
        Ok(())
    }

    fn end(self) -> Result<Asn1> {
        Ok(Asn1::Sequence(self.contents))
    }
}

impl ser::SerializeTuple for SerializeSeq {
    type Ok = Asn1;
    type Error = Error;

    fn serialize_element<T: ?Sized>(&mut self, el: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(el)?);
        Ok(())
    }

    fn end(self) -> Result<Asn1> {
        Ok(Asn1::Sequence(self.contents))
    }
}

impl ser::SerializeTupleVariant for SerializeSeq {
    type Ok = Asn1;
    type Error = Error;

    fn serialize_field<T: ?Sized>(&mut self, el: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(el)?);
        Ok(())
    }

    fn end(self) -> Result<Asn1> {
        Ok(Asn1::Sequence(self.contents))
    }
}

impl ser::SerializeStructVariant for SerializeSeq {
    type Ok = Asn1;
    type Error = Error;

    fn serialize_field<T: ?Sized>(&mut self, key: &str, el: &T) -> Result<()>
        where T: ser::Serialize
    {
        self.contents.push(to_asn1(el)?);
        Ok(())
    }

    fn end(self) -> Result<Asn1> {
        Ok(Asn1::Sequence(self.contents))
    }
}

pub fn to_asn1<T>(elem: T) -> Result<Asn1>
    where T: ser::Serialize
{
    elem.serialize(Serializer)
}

#[cfg(test)]
mod tests {
    #[derive(Serialize)]
    struct TestStruct {
        a: bool,
        b: i64,
    }
    #[test]
    fn simple_sequence() {
        use asn1::Der;
        let test = TestStruct {
            a: true,
            b: 49,
        };

        let bytes = ::to_asn1(test).unwrap()
            .der_encode().unwrap();
        assert_eq!(vec![0x30, 0x06, 0x01, 0x01, 0x01, 0x02, 0x01, 0x31], bytes);
    }
}
