use std::fmt::{self, Display, Debug};
use std::{self, result, error};

use serde::ser;

#[derive(Debug)]
pub struct Error;
pub type Result<T> = result::Result<T, Error>;

impl ser::Error for Error {
    fn custom<T>(a: T) -> Self {
        // TODO: stub
        Error
    }
}

impl error::Error for Error {
    fn description(&self) -> &str {
        // TODO: stub
        "test"
    }
}

impl Display for Error {
    fn fmt(&self, _: &mut std::fmt::Formatter) -> result::Result<(), std::fmt::Error> {
        // TODO: stub
        Ok(())
    }
}
